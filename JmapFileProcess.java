package com.yangwubing;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class JmapFileProcess {

    /**
	 * @param args
	 */
	public static void main(String[] args) {
		String fileName="C:\\Users\\Administrator\\Downloads\\20150602204037_172.17.38.26-jmap-20150602203929-log\\20150602204037_172.17.38.26-jmap-20150602203929-log"; 
	       
		String outFileName="C:\\Users\\Administrator\\Downloads\\20150602204037_172.17.38.26-jmap-20150602203929-log\\20150602203929.dump"; 

		try  
	        {  
	            byte[] buffer = new byte[16*32];
	        	
	        	DataInputStream in=new DataInputStream(  
	                               new BufferedInputStream(  
	                               new FileInputStream(fileName))); 
	        	
	        	DataOutputStream out=new DataOutputStream(  
                        new BufferedOutputStream(  
                        new FileOutputStream(outFileName))); 
	        	
	        	
	            //读取后加的内容。忽略。
	        	in.read(buffer, 0, 16*32);
	            
	        	String s = new String(buffer);
	        	
	        	System.out.println(s);  
	        	
	        	//开始读取真正内容，转存到另外一个文件中。
	        	int len = -1;
	        	do{
	        		len = in.read(buffer, 0, 16*32);
	        		if(len>0){
	        			out.write(buffer, 0, len);
	        		}
	        	}while(len!=-1);
	        	
	            in.close();
	            out.close();
	            
	            System.out.println("拷贝完成。");
	        } catch (Exception e)  
	        {  
	            e.printStackTrace();  
	        }  

	}

}
